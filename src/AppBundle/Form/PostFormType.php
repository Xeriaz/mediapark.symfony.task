<?php

namespace AppBundle\Form;

use AppBundle\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class PostFormType extends AbstractType {

	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'title', TextType::class, [
				'attr' => [
					'class' => 'form-control',
					'style' => 'margin-bottom:15px',
					'constraints' => [new Length(['min' => 3, 'max' => 255])]
				]
			] )
			->add( 'description', TextareaType::class, [
				'attr' => [
					'class' => 'form-control',
					'style' => 'margin-bottom:15px',
					'constraints' => [new Length(['min' => 3, 'max' => 255])]
				]
			] )
			->add( 'Post it', SubmitType::class, [
				'label' => 'Create Post',
				'attr'  => [
					'class' => 'btn btn-primary',
					'style' => 'margin-bottom:15px'
				]
			] );
	}

	public function configureOptions( OptionsResolver $resolver ) {

		$resolver->setDefaults([
			'data_class' => Post::class,
		]);

	}

}