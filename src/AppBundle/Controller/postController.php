<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Length;

class postController extends Controller
{
	/**
	 * @Route("/", name="post_list")
	 */
	public function listAction(Request $request)
	{
		$posts = $this->getDoctrine()
			->getRepository('AppBundle:Post')
			->findAll();

		return $this->render('post/index.html.twig', [
			'posts' => $posts
		]);
	}

	/**
	 * @Route("/post/create", name="post_create")
	 */
	public function createAction(Request $request) {
		$post = new Post;

		$form = $this->createForm(PostFormType::class, $post);

		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()) {
			$title = $form['title']->getData();
			$description = $form['description']->getData();

			$now = new\DateTime('now');

			$post->setTitle($title);
			$post->setDescription($description);
			$post->setPostCreated($now);
			$post->setAuthor($this->getUser());

			$em = $this->getDoctrine()->getManager();

			$em->persist($post);
			$em->flush();

			$this->addFlash(
				'notice',
				'Post Added'
			);

			return $this->redirectToRoute('post_list');
		}


		return $this->render('post/create.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/post/edit/{id}", name="post_edit")
	 */
	public function editAction($id, Request $request) {
		$post = $this->getDoctrine()
		             ->getRepository( 'AppBundle:Post' )
		             ->find( $id );

		if($post->getAuthor() !== $this->getUser()){
			return $this->redirectToRoute('my_posts');
		}

		$post->setTitle( $post->getTitle() );
		$post->setDescription( $post->getDescription() );

		$form = $this->createFormBuilder( $post )
		             ->add( 'title', TextType::class, [
			             'attr' => [
				             'class' => 'form-control',
				             'style' => 'margin-bottom:15px',
				             'constraints' => [new Length(['min' => 3, 'max' => 100])]
			             ]
		             ] )
		             ->add( 'description', TextareaType::class, [
			             'attr' => [
				             'class' => 'form-control',
				             'style' => 'margin-bottom:15px;height:150px;',
                             'constraints' => [new Length(['min' => 3, 'max' => 250])]

			             ]
		             ] )
		             ->add( 'Post it', SubmitType::class, [
			             'label' => 'Update Post',
			             'attr'  => [
				             'class' => 'btn btn-primary',
				             'style' => 'margin-bottom:15px'
			             ]
		             ] )
		             ->getForm();

		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$title = $form['title']->getData();
			$description = $form['description']->getData();

			$now = new\DateTime( 'now' );

			$em = $this->getDoctrine()->getManager();
			$post = $em->getRepository( 'AppBundle:Post' )->find($id);

			$post->setTitle( $title );
			$post->setDescription( $description );

			$em->flush();

			$this->addFlash(
				'notice',
				'Post Updated'
			);

			return $this->redirectToRoute('post_list');
		}

		return $this->render( 'post/edit.html.twig', [
			'post' => $post,
			'form' => $form->createView()
		] );
	}

	/**
	 * @Route("/post/details/{id}", name="post_details")
	 */
	public function detailsAction($id){
		$post = $this->getDoctrine()
             ->getRepository('AppBundle:Post')
             ->find($id);

		return $this->render('post/details.html.twig', [
			'post' => $post
		]);
	}

	/**
	 * @Route("/post/delete/{id}", name="post_delete")
	 */
	public function deleteAction($id){
		$em = $this->getDoctrine()->getManager();

		$post = $em->getRepository('AppBundle:Post')
		           ->find($id);

		if($post->getAuthor() !== $this->getUser()){
			return $this->redirectToRoute('my_posts');
		}

		$em->remove($post);
		$em->flush();

		$this->addFlash(
			'notice',
			'Post Removed'
		);

		return $this->redirectToRoute('my_posts');
	}

	/**
	 * @Route("/post/myposts/", name="my_posts")
	 */
	public function myPostsAction(Request $request)
	{
		/**
		 * @var User
		 */
		$user = $this->getUser();

		$posts = $this->getDoctrine()
		             ->getRepository('AppBundle:Post')
		             ->findBy(['author' => $user]);

		return $this->render('post/myPosts.html.twig', [
			'posts' => $posts
		]);
	}
}
